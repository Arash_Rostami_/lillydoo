<?php

namespace AppBundle\Traits;


use PowderBlue\SfContinentCountryTypeBundle\Form\Type\ContinentCountryType;

// this trait builds the content of the form for saving/making a new contact
trait FormBuilder
{

    // prepare the header & footer of the form for submission
    public function makeForm()
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('store_contact'))
            ->getForm();

        return $form = $form->createView();
    }
}