<?php


namespace AppBundle\Traits;


use Symfony\Component\Filesystem\Filesystem;

// this trait controls the upload and deleting of Contacts image file
trait ContactImage
{

    /**
     * @param $request
     * @return mixed
     * to get the file sent by AXIOS
     */
    public function getFile($request)
    {
        return $request->files->get('file');
    }

    /**
     * @param $file
     * @return string
     * to make name for the file sent by AXIOS
     */
    public function makeName($file)
    {
        return pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '-'
            . uniqid() . '.' . $file->guessExtension();
    }

    /**
     * @param $file
     * @param $newFilename
     * @return mixed
     * to move file to 'PROFILE' defined in  config.yml
     */
    public function moveFile($file, $newFilename)
    {
        return $file->move(
            $this->getParameter('profile'), $newFilename);
    }

    /**
     * @param $newFilename
     * @return mixed
     * to store the location of saved file
     */
    public function storeUrl($newFilename)
    {
        return $this->session->set('url', 'uploads/photos/' . $newFilename);
    }

    /**
     * @param $contact
     * @return mixed
     * to retrieve address of file saved from Contact table in DB
     */
    public function getUrl($contact)
    {
        return $contact->getImage();
    }

    /**
     * @param $filename
     * to delete the image of contact from the system/app
     */
    public function deleteImage($filename)
    {
        (new Filesystem())->remove($filename);
    }

    /**
     * @param $request
     * @return null
     * to save image sent by POST method through AXIOS
     */
    public function saveImage($request)
    {
        $file = $this->getFile($request);

        // this part checks if any image is sent as it is optional
        if (isset($file)) {

            $newFilename = $this->makeName($file);

            $this->moveFile($file, $newFilename);

            $this->storeUrl($newFilename);
        }
        return null;
    }


}