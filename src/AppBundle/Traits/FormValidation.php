<?php


namespace AppBundle\Traits;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validation;

// this trait controls the validation after submission of contact
trait FormValidation
{

    public $constraint, $session;


    public function __construct()
    {
        // preparing session to store the path URL of image if need be
        $this->session = new Session();

        // defining constraints for creation of new contact
        $this->constraint = new Assert\Collection([
            'name' => [
                new Assert\NotNull(), new Assert\NotBlank(['message' => 'First name cannot be blank!'])],

            'lname' => [
                new Assert\NotNull(), new Assert\NotBlank(['message' => 'Surname cannot be blank!'])],

            'birth' => [
                new Assert\NotNull(), new Assert\Date(),
                new Assert\NotBlank(['message' => 'Please choose your birthdate!'])],

            'phone' => [
                new Assert\NotNull(), new Length(['min' => 7,
                    'minMessage' => 'User Login should be at least 7 characters long',]),
                new Assert\NotBlank(['message' => 'Phone number cannot be blank!'])],

            'email' => [
                new Assert\NotNull(), new Assert\Email(['message' => 'Email is not valid!']),
                new Assert\NotBlank(['message' => 'Email address cannot be blank!'])],

            'zip' => [
                new Assert\NotNull(), new Assert\NotBlank(['message' => 'Zip code cannot be blank!'])],

            'country' => [
                new Assert\NotNull(), new Assert\Country(),
                new Assert\NotBlank(['message' => 'Country cannot be blank!'])],

            'city' => [
                new Assert\NotNull(), new Assert\NotBlank(['message' => 'city cannot be blank!'])],

            'street' => [
                new Assert\NotNull(), new Assert\NotBlank(['message' => 'Street cannot be blank!'])],

            'form' => new Assert\NotNull()
        ]);

    }

    /**
     * @param $userData
     * @return bool
     * to validate content of request
     */
    public function validate($userData)
    {
        $validator = Validation::createValidator();

        $groups = new GroupSequence(['Default', 'custom']);

        $violations = $validator->validate($userData, $this->constraint, $groups);

        return ($violations->count() > 0) ? $this->outputError($violations) : false;
    }

    /**
     * @param $violations
     * @return bool
     * to store the message of errors for Flash message toast by session
     */
    public function outputError($violations)
    {
        $errors = "";

        foreach ($violations as $violation) {
            $errors .= $violation->getMessage() . '<hr>';
        }

        $this->addFlash('message', $errors);

        return $violations->count() > 0;
    }
}