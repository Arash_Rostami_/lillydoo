<?php


namespace AppBundle\Traits;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;

// this trait acts as a helper class to instantiate different tools/containers under the hood
trait HelperModel
{
    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entityManager = $this->getDoctrine()->getManager();
    }

    /**
     * @return Request
     */
    public function getGlobals()
    {
        return Request::createFromGlobals();
    }

    /**
     * @return \Symfony\Component\PropertyAccess\PropertyAccessor
     */
    public function getProperty($array, $key)
    {
        return PropertyAccess::createPropertyAccessor()->getValue($array, $key);
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return json_decode($this->getGlobals()->getContent(), true);
    }

    /**
     * @param $contact
     * to check whether the image related to the contact already exists
     */
    public function checkImage($contact)
    {
        if ($this->session->get('url') != null) {
            // delete the previously saved file for the contact
            $this->deleteImage($this->getUrl($contact));

            // replace the previous file with this one
            $contact->setImage($this->session->get('url'));
        }
    }

    /**
     * @param $request
     * to make a new contact - process
     */
    public function storeContact($request)
    {
        // to insert in table
        $this->createSet($request);
        // to persist data in table
        $this->entityManager->persist($this->contact);
        // to clear data and session
        $this->entityManager->flush();
        $this->session->clear();
    }

    /**
     * @param $contact
     * @param $data
     * to edit contact
     */
    public function updateContact($contact, $data)
    {
        // to insert in table
        $this->editSet($contact, $data);
        // to see if the contact contains any image
        $this->checkImage($contact);
        // to persist data
        $this->entityManager->persist($contact);
        // to clear data and session
        $this->entityManager->flush();
        $this->session->clear();
    }

    /**
     * @param $contact
     * to remove contact and physical files related to it from APP
     */
    public function deleteContact($contact)
    {
        // delete the file saved
        $this->deleteImage($this->getUrl($contact));
        // delete the file from contact table
        $this->entityManager->remove($contact);

        $this->entityManager->flush();
    }

    /**
     * @param $request
     * to fill all contact details
     */
    public function createSet($request)
    {
        $this->contact->setFname($request->request->get('name'));
        $this->contact->setLname($request->request->get('lname'));
        $this->contact->setBirth($request->request->get('birth'));
        $this->contact->setPhone($request->request->get('phone'));
        $this->contact->setEmail($request->request->get('email'));
        $this->contact->setZip($request->request->get('zip'));
        $this->contact->setCountry($request->request->get('country'));
        $this->contact->setCity($request->request->get('city'));
        $this->contact->setStreet($request->request->get('street'));
        $this->contact->setImage($this->session->get('url'));
    }

    /**
     * @param $contact
     * @param $data
     * to edit all contact details
     */
    public function editSet($contact, $data)
    {
        $contact->setFname($this->getProperty($data, '[name]'));
        $contact->setLname($this->getProperty($data, '[lastName]'));
        $contact->setBirth($this->getProperty($data, '[birth]'));
        $contact->setPhone($this->getProperty($data, '[phone]'));
        $contact->setEmail($this->getProperty($data, '[email]'));
        $contact->setZip($this->getProperty($data, '[zip]'));
        $contact->setCountry($this->getProperty($data, '[country]'));
        $contact->setCity($this->getProperty($data, '[city]'));
        $contact->setStreet($this->getProperty($data, '[street]'));
    }
}