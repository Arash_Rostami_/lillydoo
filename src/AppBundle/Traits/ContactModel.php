<?php


namespace AppBundle\Traits;


use AppBundle\Entity\Contact;

// this trait acts as a contact model to control behavior in Contact table
trait ContactModel
{
    /**
     * @param $id
     * @return mixed
     * to find the requested contact based on ID
     */
    public function getContact($id)
    {
        return $this->getDoctrine()->getManager()
            ->getRepository(Contact::class)->find($id);
    }

    /**
     * @return mixed
     * to get the collection of all contacts from DB
     */
    public function getContacts()
    {
        return $this->getDoctrine()->getRepository(Contact::class);
    }

    /**
     * @return Contact
     * to instantiate new model of contact
     */
    public function makeNewContact()
    {
        return $this->contact = new Contact();
    }

    /**
     * @param $contact
     * @return array
     * to compact details of contact in one array for sending to TWIG
     */
    public function collectContactInfo($contact)
    {
        return $data =
            array(
                'name' => $contact->getFname(),
                'lastName' => $contact->getLname(),
                'birth' => $contact->getBirth(),
                'phone' => $contact->getPhone(),
                'email' => $contact->getEmail(),
                'zip' => $contact->getZip(),
                'country' => $contact->getCountry(),
                'city' => $contact->getCity(),
                'street' => $contact->getStreet(),
                'image' => $contact->getImage(),
            );
    }
}