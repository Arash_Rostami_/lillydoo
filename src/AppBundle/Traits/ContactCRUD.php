<?php


namespace AppBundle\Traits;

use AppBundle\Entity\Contact;
use Symfony\Component\Routing\Annotation\Route;

// this trait controls the CRUD functions of Contacts
trait ContactCRUD
{
    use ContactModel, HelperModel;


    public $entityManager, $contact;


    /**
     * @return mixed
     * to read contact from DB
     */
    public function read()
    {
        return $this->getContacts()->sortByName();
    }

    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * to create new contact and add to DB - Contacts table
     */
    public function create($request)
    {
        $this->getEntity();

        $this->makeNewContact();

        $this->storeContact($request);

        $this->addFlash('message', $this->contact->getFname() . " " . $this->contact->getLname() . " saved.");

        return $this->redirectToRoute('show_contact');
    }

    /**
     * @param $data
     * @return mixed
     * to edit content of Contact
     */
    public function update($data)
    {
        $this->getEntity();

        $contact = $this->getContact($this->getProperty($data, '[id]'));

        $this->updateContact($contact, $data);

        $this->addFlash('message', $contact->getFname() . " " . $contact->getLname() . " updated.");

        return $contact;
    }

    /**
     * @param $id
     * to remove the specified contact
     */
    public function delete($id)
    {
        $this->getEntity();

        $contact = $this->getContact($id);

        $this->deleteContact($contact);

        $this->addFlash('message', $contact->getFname() . " " . $contact->getLname() . " deleted.");
    }
}