<?php
/**
 * Created by PhpStorm.
 * User: Arash Rostami
 * Date: 3/14/2022
 * Time: 5:25 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Traits\ContactCRUD;
use AppBundle\Traits\ContactImage;
use AppBundle\Traits\CountryList;
use AppBundle\Traits\FormBuilder;
use AppBundle\Traits\FormValidation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;



class FormController extends Controller
{
    // to include traits from AppBundle/Traits folder
    use FormBuilder, CountryList, ContactImage, FormValidation, ContactCRUD;

    /**
     * @Route(path="/", methods={"GET"}, name="show_contact")
     * to show the main landing page
     */
    public function index(Request $request)
    {
        return $this->render('form/index.html.twig', [
            'form' => $this->makeForm(),
            'country' => $this->getCountry(),
            'contacts' => $this->read()
        ]);
    }

    /**
     * @Route(path="/", methods={"POST"}, name="store_contact")
     * to store contact information
     */
    public function store()
    {
        $request = $this->getGlobals();

        $this->saveImage($request);

        if ($this->validate($request->request->all())) {
            return $this->index($request);
        }

        return $this->create($request);
    }

    /**
     * @Route(path="/{id}", methods={"POST"}, name="show_contacts")
     * to retrieve data based on request and give information on contact
     */
    public function show($id)
    {
        $contact = $this->getContact($id);

        $data = $this->collectContactInfo($contact);

        return new jsonResponse($data);
    }

    /**
     * @Route(path="/contact/{id}", methods={"POST"}, name="edit_contact")
     * to update contact
     */
    public function edit()
    {
        $data = $this->getRequest();

        $this->saveImage($this->getGlobals());

        $this->update($data);

        return new Response(isset($file) ? 'true' : 'false');
    }

    /**
     * @Route(path="/contact/{id}", methods={"DELETE"}, name="delete_contact")
     * to remove contact together with image file related
     */
    public function destroy($id)
    {
        $this->delete($id);

        return new jsonResponse($id);
    }
}
