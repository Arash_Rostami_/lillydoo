/* controller class (powered by ALPINE JS) to manage the behave modals for show, edit, and delete and
sending binding data, events, and sending request query by AXIOS */
function modalController() {
    return {
        //for showing/hiding parts
        open: false,
        update: false,
        del: false,
        image: false,
        //for binding data set to inputs inside modals
        id: '',
        birth: '',
        city: '',
        country: '',
        email: '',
        lastName: '',
        name: '',
        phone: '',
        street: '',
        zip: '',
        url: ' ',
        // for defining CSS classes according to conditions
        modalContentShow: 'modal-content shadow-lg rounded show-mod',
        modalContentEdit: 'modal-content shadow-lg rounded edit-mod',
        modalContentDel: 'modal-content shadow-lg rounded del-mod',
        modalBodyShow: 'modal-body modal-body-show',
        modalBodyEdit: 'modal-body modal-body-edit',
        modalBodyDel: 'modal-body modal-body-del',
        modalButtonShow: 'btn modal-btn-show',
        modalButtonEdit: 'btn modal-btn-edit',
        modalButtonDel: 'btn modal-btn-del',
        init() {
            // to fix conflict between BOOTSTRAP AND ANIMATE
            $('#myModal')
                .on('show.bs.modal', function (event) {
                    $('.animate__animated').removeClass('animate__slideInRight');
                });
            // dropzone config options
            Dropzone.autoDiscover = false;
            window.onload = function () {
                // main dropzone file for grabbing image in homepage
                let dropzoneOptions = {
                    url: '/', method: "POST",
                    autoProcessQueue: true,
                    uploadMultiple: false,
                    maxFiles: 1, maxFilesize: 5,
                    acceptedFiles: 'image/*',
                    addRemoveLinks: true, paramName: "file",
                    dictDefaultMessage: 'Drop the Photo <i class="fa fa-camera-retro" aria-hidden="true"></i>',
                };
                //the dropzone file for editing image in modal
                let editDropzoneOptions = {
                    method: "POST",
                    autoProcessQueue: true,
                    uploadMultiple: false,
                    maxFiles: 1, maxFilesize: 5,
                    acceptedFiles: 'image/*',
                    addRemoveLinks: true, paramName: "file",
                    dictDefaultMessage: 'Change the Photo <i class="fa fa-camera-retro" aria-hidden="true"></i>',
                };
                //instantiating the main dropzone
                let myDropzone = document.querySelector('#myDropzone');
                new Dropzone(myDropzone, dropzoneOptions);
                //instantiating the edit dropzone
                let editDropzone = document.querySelector('#editDropzone');
                new Dropzone(editDropzone, editDropzoneOptions);
            };


            //adding event listeners for backdrop
            document.getElementById("close-button").addEventListener("click", openBackdrop);

            window.addEventListener('offline', openBackdrop);

            document.getElementById("open-button").addEventListener("click", () => location.reload());

            function openBackdrop() {
                document.getElementById('backdrop').style.display = 'block';
                document.getElementById('main').style.display = 'none';
                document.getElementById('open-button').style.display = 'block';
                document.getElementById('close-button').style.display = 'none';
            }
        },
        toggle() {
            this.open = !this.open
        },
        openModal() {
            // open MODAL
            $("#myModal").modal({show: true, keyboard: false});
        },
        closeModal() {
            this.toggle();
            // close MODAL
            $("#myModal").modal({show: false, keyboard: false});
        },
        changeUrl(id) {
            this.url = 'contact/' + id;
        },
        findMethod(el) {
            let id, method;
            let request =
                {
                    'id': 'show',
                    'edit': 'edit',
                    'delete': 'delete'
                };
            id = Object.values(el.dataset)[0];
            method = request[Object.keys(el.dataset)[0]];
            return {id, method};
        },
        view(el) {
            let {id, method} = this.findMethod(el);
            axios.post(`${id}`, {request: method,})
                .then(function (response) {
                    this.id = id;
                    this.city = response.data.city;
                    this.country = response.data.country;
                    this.birth = response.data.birth;
                    this.email = response.data.email;
                    this.lastName = response.data.lastName;
                    this.name = response.data.name;
                    this.phone = response.data.phone;
                    this.street = response.data.street;
                    this.zip = response.data.zip;
                    this.image = response.data.image;
                    this.update = (method === 'edit');
                    this.del = (method === 'delete');
                    this.toggle();
                    this.openModal();
                }.bind(this))
                .catch(error => console.log(error));
        },
        edit() {
            axios.post(`contact/${this.id}`, {
                id: this.id,
                city: this.city,
                country: this.country,
                birth: this.birth,
                email: this.email,
                lastName: this.lastName,
                name: this.name,
                phone: this.phone,
                street: this.street,
                zip: this.zip,
            }).then(function (response) {
                location.reload();
            }.bind(this))
                .catch(error => console.log(error));
        },
        destroy() {
            axios.delete(`contact/${this.id}`)
                .then(response => location.reload())
                .catch(error => console.error('There was an error!', error));
        },
        search(mode) {
            // check to whether reset or search is clicked
            let input = (mode !== 'reset')
                ? document.getElementById("myInput").value.toUpperCase()
                : 'abcdefghijklmnopqrstuvwxyz'.toUpperCase();
            let table = document.getElementById("myTable");
            let tr = table.getElementsByTagName("tr");
            // Filtering according to search
            for (let n = 0; n < tr.length; n++) {
                let td = tr[n].getElementsByTagName("td")[0];
                if (td) {
                    let txt = td.textContent || td.innerText;
                    // check to whether reset or search is clicked
                    (mode !== 'reset') ?
                        // hide those row that are not similar to search value
                        (txt.toUpperCase().indexOf(input) <= 0)
                            ? tr[n].style.display = "none"
                            : tr[n].style.display = "" :
                        // reload the list content
                        (txt.toUpperCase().includes(input))
                            ? tr[n].style.display = "none"
                            : tr[n].style.display = "";
                }
            }
        },
        sortTable(mode) {
            let table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("myTable");
            switching = true;
            /* Make a loop that will continue until
            no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.rows;
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1); i++) {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = rows[i].getElementsByTagName("TD")[0];
                    y = rows[i + 1].getElementsByTagName("TD")[0];
                    // Check if the two rows should switch place:
                    if (mode === 'asc') { // if ascending
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            shouldSwitch = true;
                            break;
                        }
                    } else if (mode === 'des') {// if descending
                        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                            shouldSwitch = true;
                            break;
                        }
                    }
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }
    }
}
